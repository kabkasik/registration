package com.example.registration;

import com.example.registration.models.dto.ClientDto;
import com.example.registration.models.dto.PhoneDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class RegistrationApplicationTests {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @LocalServerPort
    int randomServerPort;

    @Test
    void clientAdd() {


        List phoneNumbers = new ArrayList<PhoneDto>();
        phoneNumbers.add(new PhoneDto(generatePhone()));
        phoneNumbers.add(new PhoneDto(generatePhone()));
        ClientDto clientDto = new ClientDto("Popov", "Alex", generatePhone(), phoneNumbers);

        RequestEntity.BodyBuilder builder = RequestEntity
                .method(HttpMethod.POST, URI.create("http://localhost:" + randomServerPort + "/client/add"))
                .contentType(MediaType.APPLICATION_JSON);

        ResponseEntity<String> response = testRestTemplate.exchange(builder.body(clientDto), String.class);

        Assert.isTrue(200 == response.getStatusCodeValue(), "Returned code OK");
        Assert.isTrue(Integer.parseInt(response.getBody()) >= 0, "Client added to DB");
    }

    private static String generatePhone() {
        Random rnd = new Random();

        int num1 = rnd.nextInt(899) + 100;
        int num2 = rnd.nextInt(899) + 100;
        int num3 = rnd.nextInt(8999) + 1000;

        return "+7(" + num1 + ")" + "-" + num2 + "-" + num3;
    }

}
